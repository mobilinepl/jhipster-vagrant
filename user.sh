#!/bin/bash

# configuration as a vagrant user

# to avoid problems with git connection timeout
git config --global url."https://".insteadOf git://
mkdir /home/vagrant/workspace