#!/bin/bash
export JAVA_HOME=/opt/jdk8
export JRE_HOME=$JAVA_HOME/jre
export PATH=$PATH:$JAVA_HOME/bin:$JAVA_HOME/jre/bin
