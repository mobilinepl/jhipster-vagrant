#!/bin/bash

# java8 repositories
add-apt-repository ppa:webupd8team/java -y
apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
apt-get -y install oracle-java8-installer && apt-get clean

update-java-alternatives -s java-8-oracle
echo "export JAVA_HOME=/usr/lib/jvm/java-8-oracle" >> /home/vagrant/.bashrc

apt-get update
apt-get -y install apache2 tomcat7 oracle-java8-installer git unzip mc build-essential maven zip bzip2 postgresql samba
# to run tomcat7 with java8
ln -s /usr/lib/jvm/java-8-oracle /usr/lib/jvm/default-java

# password for mysql user
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
apt-get -y install mysql-server

# password for postgres user
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'vagrant';"

# copy samba config
cp /vagrant/smb.conf /etc/samba/

# nodejs installation
cd /opt/
wget https://nodejs.org/dist/v0.12.7/node-v0.12.7-linux-x64.tar.gz
tar xvf node-v0.12.7-linux-x64.tar.gz
ln -s node-v0.12.7-linux-x64 /opt/node
cp /vagrant/profile.d/node.sh /etc/profile.d/

# groovy installation
wget http://dl.bintray.com/groovy/maven/groovy-sdk-2.4.3.zip
unzip groovy-sdk-2.4.3.zip
ln -s groovy-2.4.3 /opt/groovy
cp /vagrant/profile.d/groovy.sh /etc/profile.d/

# grails installation
wget https://github.com/grails/grails-core/releases/download/v3.0.3/grails-3.0.3.zip
unzip grails-3.0.3.zip
ln -s grails-3.0.3.zip /opt/grails
cp /vagrant/profile.d/grails.sh /etc/profile.d/

# gradle installation
wget https://services.gradle.org/distributions/gradle-2.5-all.zip
unzip gradle-2.5-all.zip
ln -s gradle-2.5-all.zip /opt/gradle
cp /vagrant/profile.d/gradle.sh /etc/profile.d/

# apache tomcat7
wget http://ftp.ps.pl/pub/apache/tomcat/tomcat-7/v7.0.63/bin/apache-tomcat-7.0.63.tar.gz
tar xvf apache-tomcat-7.0.63.tar.gz
ln -s apache-tomcat-7.0.63 /opt/apache-tomcat7
cp /vagrant/profile.d/tomcat7.sh /etc/profile.d/
cp /vagrant/init.d/tomcat7 /etc/init.d/
cp /vagrant/tomcat-users.xml /opt/apache-tomcat7/conf/