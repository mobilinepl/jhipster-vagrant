#### Access to vagrant machine:

* Access to linux machine via ssh: `localhost:2222`, login: vagrant/vagrant (Putty)
* Browse running project: `localhost:8228`
* Access to jhipster project files in workspace: `\\192.168.33.10\workspace`
* **A good idea is to map network share with project.**
* Mysql access: root/vagrant
* Postgresql access: vagrant/vagrant

#### Creating a project:
* Create jhipster project in workspace: `/home/vagrant/workspace`
* Use **maven, java8**
* Open project in IDE from `\\192.168.33.10\workspace`
* Run project from terminal: `mvn spring-boot:run`
* **Enjoy** :)